<?php

class Loader {
	
	public static function autoload($class) {

		$classPath = str_replace("\\", "/", $class);

		$file = ROOT . $classPath . ".php";

		require_once $file;

	}

}

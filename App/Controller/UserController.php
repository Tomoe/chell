<?php

namespace App\Controller;

use App\Core\Response;
use App\Core\Debug;

class UserController {

	public function Index() {
		return Response::view("User/index", array(
			"user" => array(
				"username" => "test",
			)
		));
	} 

	public function Detail($id) {
		return Response::view("User/detail", array("id" => $id));
	} 

}
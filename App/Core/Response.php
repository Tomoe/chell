<?php

namespace App\Core;

use App\Core\View;

class Response {

	public static function view($viewPath, $model = null) {
		if($model == null) {
			new View($viewPath);
		} else {
			new View($viewPath, $model);
		}
	}

}
<?php

namespace App\Core\Template;

use App\Core\Template\Engine;

class Template {

    /**
     * 樣板引擎實體
     * @var Engine
     */
    protected $engine;

    /**
     * 樣版名稱
     * @var string
     */
    protected $file;

    /**
     * 父樣板
     * @var Template;
     */
    protected $layout;

    /**
     *
     * @var array
     */
    protected $data = array();

    /**
     *
     * @var array
     */
    protected $func = array();

    /**
     * 建構式
     * @param Engine $engine
     * @param $name string
     */
    public function __constructor(Engine $engine, $file) {
        $this->engine = $engine;
        $this->name = $file;
    }

    public function render() {

        // 開始緩存
        ob_start();

        // 輸出緩存內容
        $content = ob_get_clean();

        return $content;
    }

    public function layout() {

    }

    public function start() {

    }

    public function stop() {

    }

}
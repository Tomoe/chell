<?php

namespace App\Core\Template;

/**
 * 樣版引擎　類
 * @package App\Core\Template
 */
class Engine {

    /**
     * 預設放置樣板的資料夾
     * @var Directory
     */
    protected $directory;

    /**
     * 樣板副檔名
     * @var string
     */
    protected $extension;

    /**
     * 建構式
     * @param string $directory
     * @param string $extension
     */
    public function __constructor($directory = null, $extension = 'php') {

        $this->directory = new Directory($directory);
        $this->extension = $extension;

    }

}
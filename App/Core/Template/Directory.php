<?php

namespace App\Core\Template;

/**
 * Directory 類
 * @package App\Core\Template
 */
class Directory {

	protected $path;

    /**
     * 建構式
     * @param string $path
     */
	public function __construct($path = null) {
		$this->set($path);
	}


    /**
     * 設定路徑
     * @param string $path
     * @return Directory
     */
	public function set($path) {
		// 檢查資料夾是否存在
        return $this;
	}

    /**
     * 取得路徑
     * @return string
     */
	public function get() {
		return $this->path;
	}

}
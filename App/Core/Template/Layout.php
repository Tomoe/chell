<?php

namespace App\Core\Template;

class Template {

	public static function section($viewPath) {
		self::includeView($viewPath);
	}

	public static function layout($viewPath) {
		self::includeView($viewPath);
	}

	public static function includeView($path) {
		include "App/View/" . $path . ".php";
	}

	public static function end() {

	}

}
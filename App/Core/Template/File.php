<?php

namespace App\Core\Template;

class File {

    protected $path;

    public function __constructor($path) {
        $this->path = $path;
    }

    public function isExist() {
        return is_file($this->path);
    }

}
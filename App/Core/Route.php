<?php

//////////////
//	路由類
//////////////

namespace App\Core;

use App\Core\Debug;

class Route {

	// 網址
	protected $url;

	// GET | POST
	protected $mothod;

	// 目標
	protected $target;

    // 參數
    protected $params;

	public function __construct($resource, $target, $method) {

		$this->url = $resource;
		$this->method = $method;
        $this->params = array();

        $this->parseTarget($target);

	}

    public function match($url) {
        if($url != '/') {
            $url = rtrim($url,'/');
        }
		Debug::prePrint($url);
        
        $matches = array();

        // 匹配成功
        if(preg_match($this->getRegex(), $url, $matches)) {
            // 填充參數
            foreach($matches as $key => $value) {
                if(array_key_exists($key, $this->params)) {
                    $this->addParam($key, $value);
                }
            }
            // 
            return true;
        // 失敗
        } else {
            return false;
        }
    }

    // 轉換 Url 成 Regular Expression
    public function getRegex() {

        // 匹配 {$paramName}
        $pattern = '#{([a-z][a-zA-Z0-9_]*)}#'; 
        $regex = $this->url;

        // 取得所有參數型式
        preg_match_all(
            $pattern,
            $regex,
            $matches,
            PREG_SET_ORDER
        );

        foreach ($matches as $match) {
            $name = $match[1];
            $this->params[$name] = null;
            $pattern = $this->getParameterRegex($name);
            $regex = str_replace("{{$name}}", $pattern, $regex);
        }

        return '#' . $regex . '$#';

    }

    public function getParameterRegex($name) {
        return "(?<{$name}>[^/]+)";
    }

	public function getUrl() {
		return $this->url;
	}

	public function getMethod() {
		return $this->method();
	}

	public function getTarget() {
		return $this->target;
	}

    public function getController() {

        return $this->target->controller;
    }

    public function getAction() {
        return $this->target->action;
    }

    public function getParams() {
        return $this->params;
    }

    public function getParam($name) {
        if(array_key_exists($name, $this->params)) {
            return $this->params[$name];
        } else {
            return null;
        }
    }

    public function addParam($name, $value) {
        $this->params[$name] = $value;
    }

    public function parseTarget($target) {
        $_target = explode("@", $target);
        $this->target["controller"] = $_target[0];
        $this->target["action"] = $_target[1];
        $this->target = (object) $this->target;
    }

}
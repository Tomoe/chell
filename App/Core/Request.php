<?php

//////////////
//	請求類
//////////////

namespace App\Core;

use App\Core;

class Request {

	public static function method() {
		return $_SERVER['REQUEST_METHOD'];
	}

	public static function url() {

		$rootPath = str_replace(
			$_SERVER["DOCUMENT_ROOT"],
			"",
			str_replace("\\", "/", ROOT)
		);

		$requestUri = isset($_SERVER["REQUEST_URI"]) ? $_SERVER["REQUEST_URI"] : '/';
		$requestUri = str_replace($rootPath, "", $requestUri);

		return $requestUri;
	}

}
<?php

namespace App\Core;

use App\Core\Constant;

use App\Core\View\Layout;

class View {

	protected $view;
	protected $model;

	public function __construct($view, $model = null) {
		$this->view = $view;
		$this->model = $model;
		$this->render();
	}

	public function render() {


		// 置入使用者定義的變數
		if (is_array($this->model) && !empty($this->model)) {
		    extract($this->model);
		}

		ob_start();

		Layout::includeView($this->view);

		echo ob_get_clean();
	}

}
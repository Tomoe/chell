<?php

//////////////
//	路由器類
//////////////

namespace App\Core;

use App\Core\Route;

class Router {

	private $targetSeparator = "@";

	// 定義好的 Route
	private $routes = array();

	private $requestUrl;

	private $requestMethod;

	public function __construct() {
		$this->requestUrl = Request::url();
		$this->requestMethod = Request::method();
	}

	public function add($route) {
		$this->add($route);
	}

	// Get Route
	public function get($url, $target) {
		$this->routes[] = new Route($url, $target, "GET");
	}

	// Post Route
	public function post($url, $target) {
		$this->routes[] = new Route($url, $target, "POST");
	}

	public function match() {
		foreach($this->routes as $route) {
			if($route->match($this->requestUrl)) {
				return $route;
			}
		}
		return false;
	}

	public function showRoutes() {
		echo "<pre>";
		print_r($this->routes);
		echo "</pre>";
	}

	public function currentUrl() {

	}

}
<?php

//////////////
//	檔案類
//////////////

namespace App\Core;

class File {

	protected $file;

	protected $path;

	protected $extension;

	protected $name;

	protected $fileName;

	protected $filePath;

	public function __construct($file,$pathPart) {
		$this->file = $file;
		$this->path = $pathPart['dirname'];
		$this->fileName = $pathPart['basename'];
		$this->extension = $pathPart['extension'];
		$this->name = $pathPart['filename']; 
		$this->filePath = $this->path .'/'. $this->fileName;
	}
	
	public static function loadByPath($filePath) {
		if(!file_exists($filePath)) {
			echo 'File not found,and create new File now!';
		} 
		$file = fopen("$filePath", "a+");
		$pathPart = pathinfo("$filePath");
		return new File($file, $pathPart);
	}

	public static function loadByInstance($file) {
		
	}

	public function getMsg() {
		$message = Array();
		fseek($this->file,0);
		if(flock($this->file, LOCK_EX)) {
			while (!feof($this->file)) {
		        $message[] = fgets($this->file);
	    	}
	    	flock($this->file, LOCK_UN);
			
    	}
    	
    	return $message;
	}

	public function addMsg($message) {
			if(flock($this->file, LOCK_EX)) {
				if(!empty($message)){
					fwrite($this->file , $message."<br />");
					flock($this->file, LOCK_UN);
				}
			}
		
		
	}

	public function move($newPath) {
		$oldPath = $this->path.'/'.$this->fileName;
		if(fclose($this->file) == true){
			if($oldPath == $newPath) {
				echo "[moveFilePath] : two path are same";
			} else { 
				echo rename("$oldPath","$newPath");
			}
		}else{
			echo "[moveFilePath] : file not close";
		}
		
	}

	public function modifyFileName($newName){
		$oldName = $this->fileName;
		if(fclose($this->file) == true){
			if($oldName == $newName) {
				echo "[modifyFileName] : two name are same";
			} else { 
				rename("$oldName", "$newName");
			}
		}else{
			echo "[modifyFileName] : file not close";
		}
	}

	//無法清除資料,所以直接透過別的方式再次打開資料,此種打開方式為用不同方式讀取,他會去清除原先的
	public function clearAllMsg(){ 
		if(flock($this->file, LOCK_EX)){
			$this->file = fopen("$path", "w");
			flock($this->file, LOCK_UN);
		}
		$this->file = fopen("$this->filePath", "a+");
	}

	public function deleteFile(){
		echo (unlink("$this->filePath"))? "Successfully Deleted" : "Could not delete file";
	}

	public function showKeyMsg($key, $arr){
		self::printMsg("/$key/i",$arr);
	}


	public function showAllMsg($arr) {
		self::printMsg("",$arr);	
	}

	public function dispose() {
		fclose($this->file);
	}


	private function printMsg($key, $arr){
		if(!empty($arr)){
			if(flock($this->file, LOCK_EX)) {
				if(empty($key)){
					foreach($arr as $val) {
						echo $val;
					}
				}else{
					foreach($arr as $val) {
						if(preg_match("$key", $val)){
							echo  $val;
						}
					}
				}
				flock($this->file, LOCK_UN);
			}		

		}else{
			echo 'no data is array';
		}
	}

}

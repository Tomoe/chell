<?php

//////////////
//  路由器類
//////////////

namespace App\Core;

use App\Core\File;
use App\Core\Constant;
class Log {

    // 資訊
    public static function info($message) {
        // 產生訊息
        $logMessage = "[Info] " . $message;
        // 存入檔案
        $pathName = Constant::LOG_FILE;
        $pathPath = ROOT . $pathName;
        $file = File::loadByPath("$pathName");
        $file->saveMsg($logMessage);
        $file->dispose();
    }

    // 錯誤
    public static function error($message) {
    	$logMessage = "[Error] " . $message;
        // 存入檔案
        $pathName = Constant::LOG_FILE;
        $pathPath = ROOT . $pathName;
        $file = File::loadByPath("$pathName");
        $file->saveMsg($logMessage);
        $file->dispose();
    }

}

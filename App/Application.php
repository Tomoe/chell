<?php

//////////////
//	應用類
//////////////

namespace App;

use App\Core\Route;
use App\Core\Router;
use App\Core\Debug;
use APP\Core\Constant;

class Application {

	private $router;

	public function __construct() {

		$this->router = new Router();

        // 匹配順序問題
        $this->router->get("/users/{id}", "UserController@Detail");
		$this->router->get("/users", "UserController@Index");
        $this->router->get("/", "HomeController@Index");

        $route = $this->router->match();

        if($route) {
           
            $controllerName = $route->getController();
            $actionName = $route->getAction();
            $class = "App\\Controller\\$controllerName";

            $controller = new $class();
            $controller->$actionName($route->getParams());
        } else {
            // return http404
        }


	}

}
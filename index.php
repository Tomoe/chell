<?php

define('ROOT', __DIR__ . DIRECTORY_SEPARATOR);
define('APP', ROOT . 'APP' . DIRECTORY_SEPARATOR);

// Bootstrap
require_once ROOT . 'Bootstrap/Loader.php';

spl_autoload_register('Loader::autoload');

$app = new App\Application();
